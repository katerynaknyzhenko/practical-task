import {NavLink, useHistory} from "react-router-dom";
import React, {BaseSyntheticEvent} from "react";
import '../App/App.scss';
import {ErrorComponent} from "../Error/Error";
import classNames from 'classnames';
import {translate} from "../i18n/translate";
import PropTypes from "prop-types";
import {BASE_URL} from "../constants";
import {Context} from "../Context";
import {client, handleErrorsByResponse} from "../api_service/API_client";
import './login.scss';
import {useSelector} from "react-redux";

// @ts-ignore
export function LogIn ({language, chooseLanguage}) {
    const [login, setLogin] = React.useState<string>('');
    const [password, setPassword] = React.useState<string>('');
    const {setCookie} = React.useContext(Context);
    let history = useHistory();

    const errorFromStore = useSelector((state: any) => state.errors);

    const handleInputChange = (e: BaseSyntheticEvent, set: any) => {
        return set(e.target.value);
    }

    const isFieldMistaken =  (key: string) => errorFromStore.some((item: any) => item.includes(key));

    const isErrorExist = errorFromStore.length > 0;

    const renderError = (field: string, key: string) => {
        if (isErrorExist && !field) {
            return <ErrorComponent errors={['Required field is not filled']}/>
        }
        else if (isErrorExist && isFieldMistaken(key)) {
            return <ErrorComponent errors={[errorFromStore]}/>
        }
    }

    const submitLogin =  (e: BaseSyntheticEvent) => {
        e.preventDefault()
        const callApi = async () => {

            const response = await client.post(`${BASE_URL}/auth/login`,
            {"login": login, "password": password}, null);


        const ifResponse200_300 = async () => {
            const parsedResponse = await response?.json();
            setCookie('token', parsedResponse.accessToken);
            await history.push('/home');
        }

            handleErrorsByResponse(response, ifResponse200_300, null)
    }
    callApi()}

    const handleLangChoose = (value: string) => {
    if (value === 'en') {
        chooseLanguage(value);
    }
        if (value === 'uk') {
            chooseLanguage(value);
        }
    }
    const english = language === 'en';
    const ukrainian = language === 'uk';


    return (
        <div className="centered-wrapper">
            <div className="locales-box">
                <div className="locale">
                    <span onClick={() => handleLangChoose('en')}>EN</span>
                    {english && <hr/>}
                </div>
                <div className="locale">
                    <span onClick={() => handleLangChoose('uk')}>UK</span>
                    {ukrainian && <hr/>}
                </div>
            </div>
            <div className="title-login">{translate('login-title')}</div>
            <form className="form" action="" method="post" name="log_form">
                <div className="form-wrapper login">
                    <div className="flex-parent">
                        <div className="flex-son">
                            <div className="flex-grandson">
                                <label className="form-label" htmlFor="log">{translate('login-label')}</label>
                                <input
                                    onChange={(e) => handleInputChange(e, setLogin)}
                                    className={classNames('login-input', {'redBorders': isErrorExist &&
                                             !login})}
                                    required maxLength={20} name="log" type="text"/>
                            </div>
                            {renderError(login, 'login')}
                        </div>
                    <div className="flex-son">
                        <div className="flex-grandson">
                            <label className="form-label" htmlFor="pas">{translate('password-label')}</label>
                            <input
                                id='pas'
                                onChange={(e) => handleInputChange(e, setPassword)}
                                className={classNames('login-input pas',
                                    {'redBorders': isErrorExist && (isFieldMistaken('password')  || !password)})}
                                required maxLength={20} name="pas" type="text"/>
                        </div>
                        {renderError(password, 'password')}
                    </div>
                    </div>
                    <button type="submit" onClick={submitLogin}
                            className="enter-button" name="pas">{translate('login-button')}</button>
                </div>
            </form>
            <div className="registration">
                <NavLink  to='/registration' style={{textDecoration: 'none'}}>
                    <span className="registration_link">{translate('registration-button')}
                    </span>
                </NavLink>
            </div>
        </div>
    )
}
LogIn.propTypes = {
    language: PropTypes.string,
    chooseLanguage: PropTypes.any,
}