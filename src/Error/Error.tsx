import PropTypes from "prop-types";
import './error.scss';

interface Error {
    errors: string[]
}
export const ErrorComponent = ({errors}: Error) => {
    const errorMessages = errors.map((error: string) => {
      return <div>{error}</div>});
    return (
        <div className='error-message'>{errorMessages}</div>
    )

}
ErrorComponent.prototype = {
    errors: PropTypes.array,
}