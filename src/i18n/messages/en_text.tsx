import {LOCALES} from "../locales";

export default {
    [LOCALES.EN]: {
        'login-title': 'Your private photo collection',
        'login-label': 'Login:',
        'password-label': 'Password:',
        'login-button': 'Log in',
        'registration-button': 'Registration',
        'registration-title': 'Registration',
        'name': 'Name:',
        'surname': 'Surname:',
        'age': 'Age:',
        'welcome home': 'Welcome home!',
        'ask': 'Ask your question',
        'select-avatar': 'Select as avatar',
        'upload-photo': 'Upload your photo',
        'ask smth': 'Ask us smth',
        'done': 'Done',


    }
}