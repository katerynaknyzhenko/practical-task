import en from './en_text';
import uk from  './uk_text';

export default {
    ...en, ...uk
}