import {LOCALES} from "../locales";

export default {
    [LOCALES.UK]: {
     'login-title': 'Ваша власна фото-колекція',
        'login-label': 'Логін:',
        'password-label': 'Пароль:',
        'login-button': 'Увійти',
        'registration-button': 'Зареєструватися',
        'registration-title': 'Реєстрація',
        'name': 'Ім`я:',
        'surname': 'Прізвище:',
        'age': 'Вік:',
        'welcome home': 'Ласкаво просимо!',
        'ask': 'Задайте питання',
        'select-avatar': 'Встановити як аватар',
        'upload-photo': 'Завантажити фото',
        'ask smth': 'Задайте нам запитання',
        'done': 'Готово'
    }
}