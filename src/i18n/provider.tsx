import React, {Fragment} from "react";
import {IntlProvider} from "react-intl";
import messages from './messages/lang-wrapper';

interface Props {
    children: React.ReactNode,
    locale: string,
}
export const Provider = ({children, locale}: Props) => {
    return (<IntlProvider locale={locale} textComponent={Fragment} messages={messages[locale]}>
        {children}
    </IntlProvider>
    )
}