import React from "react";
import {FormattedMessage} from "react-intl";

export const translate = (id: string) => {
   return  (<FormattedMessage id={id}/>)
}