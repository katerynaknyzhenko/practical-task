import 'jsdom-global/register';
import {mount, shallow, ShallowWrapper} from "enzyme";
import App from "./App/App";
import React from "react";
import {Home} from "./Home/Home";
import {Registration} from "./Registration/Registration";
import {Chat} from "./Chat/Chat";
import {LogIn} from "./Login/LogIn";
import {ErrorComponent} from "./Error/Error";
import {CarouselBox} from "./Home/Carousel/CarouselBox"
import {fireEvent, getByTestId} from "@testing-library/react";
import {render, cleanup} from '@testing-library/react';
import messages from './i18n/messages/lang-wrapper';

const Intl = jest.genMockFromModule('react-intl');

const intl = {
    formatMessage: ({defaultMessage}: any = messages['en']) => defaultMessage
};

// @ts-ignore
Intl.injectIntl = (Node: any) => (props: any) => <Node {...props} intl={intl}/>;

module.exports = Intl;

const loginProps = {
    language: 'en',
    choseLanguage: () => {},
}

const setUpApp = () => shallow(<App />);
const testHome = () => shallow(<Home/>);
const testLogin = () => shallow(<LogIn {...loginProps}/>);
let login: any;

Object.defineProperty(window.document, 'cookie', {
    get: jest.fn().mockImplementation(() => { return ''; }),
    set: jest.fn().mockImplementation(() => {}),
});

describe('rendering App', () => {
    let mainComponent: React.Component<any, {}, any> | ShallowWrapper<any, {}, React.Component<{}, {}, any>> | any;
    beforeEach(() => {
        mainComponent = setUpApp();
    });
    it ('should render App component', () => {
        expect(mainComponent).toMatchInlineSnapshot(`ShallowWrapper {}`);

    })
})
describe('rendering Home', () => {
    let homeComponent: any;
    beforeEach(() => {
        const spy = jest.spyOn(React, 'useContext');
        spy.mockReturnValue(['token=1']);
        shallow(<Home/>)
        expect(spy).toBeCalled();
    });
    it ('should render Home component', () => {
        homeComponent = testHome();
        expect(homeComponent).toMatchInlineSnapshot(`ShallowWrapper {}`);
    })

})
describe('rendering components', () => {
    it ('should render Registration component', () => {
        shallow(<Registration/>)
    })
    it ('should render Chat component', () => {
        shallow(<Chat/>)
    })
    it ('should render Login component', () => {
        const spy = jest.spyOn(React, 'useContext');
        spy.mockReturnValue(['token=1']);
        shallow(<LogIn/>)
        expect(spy).toBeCalled();
    })
    it ('should render Error component', () => {
        shallow(<ErrorComponent errors={[]}/>)
    })
    it ('should render Carousel component', () => {
        shallow(<CarouselBox/>)
    })
})


describe('how inputs in Login work', () => {
    beforeEach(() => {
        const spy = jest.spyOn(React, 'useContext');
        spy.mockReturnValue(['token=1']);
        shallow(<LogIn/>)
        login = testLogin();
    });
    it('calling states in LogIn', () => {
        const setState = jest.fn();
        const useStateMock: any = (initState: any) => [initState, setState];
        jest.spyOn(React, 'useState').mockImplementation(useStateMock);
        const initial = {login: '1', password: '2'};
        setState(initial);
        setState({login: '3', password: '4'})
        expect(setState).toHaveBeenCalledTimes(2);
        expect(setState).toHaveBeenCalledWith({login: '1', password: '2'});
        expect(setState).toHaveBeenCalledWith({login: '3', password: '4'});
    })
    it ('check handleInputChange', () => {
        const onSearchMock = jest.fn();
        const event = {
            preventDefault() {},
            target: { value: 'the-value' }
        };
        login.find('#pas').simulate('blur', event);
        onSearchMock(event.target.value);
        expect(onSearchMock).toBeCalledWith('the-value');
    })
    afterEach(() => {
        jest.clearAllMocks();
        cleanup();
    });
    it ('Inputing text updates the state',  () => {

        // @ts-ignore
        const loginWithIntl = Intl.injectIntl(<LogIn {...loginProps}/>);
        // @ts-ignore
        const {getByLabelText, debug} = render(<LogIn/>, { wrapper: Intl.injectIntl },);
     debug();

        const label =  getByLabelText('f');
        fireEvent.change(label, {target: {value: 'kitty' } } )
        const val: any = getByTestId(login, "pas");
        expect(val.value).toBe('kitty')
    });
})
