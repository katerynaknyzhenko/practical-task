import '../App/App.scss';
import React, {BaseSyntheticEvent} from "react";
import {useHistory} from "react-router-dom";
import '../App/App.scss';
import {ErrorComponent} from "../Error/Error";
import classNames from "classnames";
import {translate} from "../i18n/translate";
import {BASE_URL} from "../constants";
import {client, handleErrorsByResponse} from "../api_service/API_client";
import './registration.scss';
import {useSelector} from "react-redux";


export function Registration () {
const [name, setName] = React.useState('');
const [surname, setSurname] = React.useState('');
const [age, setAge] = React.useState(0);
const [login, setLogin] = React.useState('');
const [password, setPassword] = React.useState('');
const [email, setEmail] = React.useState('');
let history = useHistory();

    const errorFromStore = useSelector((state: any) => state.errors);

const handleInputChange = (e: BaseSyntheticEvent, setState: any) => {
   return setState(e.target.value);
}

const handleSubmit = async (e: BaseSyntheticEvent) => {
    e.preventDefault();
    errorFromStore.length && e.preventDefault()
                const response = await client.post(`${BASE_URL}/auth/register`,
                    {"username": name, "surname": surname, "password": password, "email": email,
                            "age": age, "login": login}, null)

                const ifResponse200_300 = async () => {
                    history.push('/')
                }
                await handleErrorsByResponse(response, ifResponse200_300, null)

}

    const isFieldMistaken =  (key: string) => errorFromStore.some((item: any) => item.includes(key));

const isErrorExist = errorFromStore.length > 0;

const renderError = (field: string, key: string) => {
    if (isErrorExist && !field) {
        return <ErrorComponent errors={['Required field is not filled']}/>
    }
    else if (isErrorExist &&  isFieldMistaken(key)) {
        return <ErrorComponent errors={[errorFromStore]}/>
    }
}

    return (
    <div className='centered-wrapper registration'>
        <div className="title">{translate('registration-title')}</div>
        <form className="form" action="" method="post" name="reg_form">
            <div className="form-wrapper">
                <div className="flex-parent">
                    <div className="flex-son">
                        <div className="flex-grandson">
                            <label className="form-label" htmlFor="name">{translate('name')}</label>
                            <input
                                required className={
                                    classNames('registration-input', {'redBorders': isErrorExist && ( isFieldMistaken('username') || !name)})}
                                 maxLength={20} name="name" type="text" onChange={(e) => handleInputChange(e,setName)} />
                        </div>
                       {renderError(name, 'username')}
                    </div>
                    <div  className="flex-son">
                        <div  className="flex-grandson">
                            <label className="form-label" htmlFor="surname">{translate('surname')}</label>
                            <input onChange={(e) => handleInputChange(e,setSurname)}
                                   className='registration-input'
                                   maxLength={20} name="surname" type="text"/>
                        </div>
                    </div>
                    <div className="flex-son">
                        <div className="flex-grandson">
                            <label className="form-label" htmlFor="age">{translate('age')}</label>
                            <input onChange={(e) => handleInputChange(e,setAge)}
                                   className='registration-input'
                                   maxLength={20} name="age" type="text"/>
                        </div>
                    </div>
                    <div className="flex-son">
                        <div className="flex-grandson">
                            <label className="form-label" htmlFor="email">Email:</label>
                            <input required onChange={(e) => handleInputChange(e,setEmail)}
                                   className={
                                       classNames('registration-input', {'redBorders': (isErrorExist && (isFieldMistaken('email') || !email))})}
                                   maxLength={30} name="email" type="text"/>
                        </div>
                        {renderError(email, 'email')}
                    </div>
                    <div className="flex-son">
                        <div className="flex-grandson">
                            <label className="form-label" htmlFor="login">{translate('login-label')}</label>
                            <input required onChange={(e) => handleInputChange(e,setLogin)}
                                   className={classNames('registration-input', {'redBorders': !login && isErrorExist})}
                                    maxLength={20} name="login" type="text"/>
                        </div>
                        {renderError(login, 'login')}
                    </div>
                    <div className="flex-son">
                        <div className="flex-grandson">
                            <label className="form-label" htmlFor="password">{translate('password-label')}</label>
                            <input required onChange={(e) => handleInputChange(e,setPassword)}
                                   className={classNames('registration-input', {'redBorders': (isErrorExist && (isFieldMistaken('password') || !password))})}
                                   maxLength={20} name="password" type="text"/>
                        </div>
                        {renderError(password, 'password')}
                    </div>
                </div>
                        <button onClick={handleSubmit} className="enter-button" type='submit' name="pas">
                            {translate('done')}
                        </button>
            </div>
        </form>
    </div>
)
}