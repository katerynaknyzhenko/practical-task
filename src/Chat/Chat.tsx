import  pink from '../pictures/pexels-photo-9063025.jpeg';
import button from '../pictures/buttonasbutton.jpeg';
import back from '../pictures/back.jpeg';
import React from "react";
import { translate } from '../i18n/translate';
import './chat.scss';

export const Chat = () => {
    const [messages, setMessage] = React.useState<any[]>(['hi', 'how are you?']);

    const inNotEmptyString = (event: any) => event.target.value &&  event.target.value.trim().length;

    const handleTextfieldChange = (event: any) => {
        inNotEmptyString(event) &&  messages.push(event.target.value);
    }

    const handleKeyDown = (e: any) => {
        if (e.key === 'Enter' &&  inNotEmptyString(e)) {
            handleTextfieldChange(e)
            handleSubmit(e);
        }
    }

    const handleSubmit = (event: any) => {
         event.preventDefault();
        setMessage([ ...messages]);
        if (event.key !== 'Enter') {
            event.target.reset()
        }
        event.target.value = ''
        return messages;
    }
    const block = document.getElementById("messages-block");

    block  && (block.scrollTop = block.scrollHeight);

    const chosenAvatar = localStorage.getItem('ava');

    const avatar = chosenAvatar ?`${chosenAvatar}` : pink;
    return (
        <div className='chat-wrapper'>
            <header className="header">
                <input className="back-button" type='image'
                       src={back} width={25} height={25} onClick={() => window.history.back()}/>
                <span>{translate('ask smth')} &#128578;</span>
                <span/>
            </header>
            <div className='messages-container' id='messages-block' >
            {messages.map((el => (
               <div id='sms' className="message">
                    <img alt='avatar' width='50' height='50'
                         src={avatar}/>
                    <span>{el}</span>
                </div>
            )))}
            </div>
            <form className="commentForm" onSubmit={handleSubmit}>
            <textarea name='in' className='type-zone' onBlur={handleTextfieldChange} onKeyDown={handleKeyDown}/>
                <button type='submit' className="send-sms">
                    <img width='40px' height='40px' alt='button' src={button}/>
                </button>
            </form>
        </div>
    )
}
