import {Carousel, CarouselItem} from "react-bootstrap";
import React from "react";
import basket from "../../pictures/24873581.jpg";
import PropTypes from "prop-types";
import {BASE_URL} from "../../constants";
import "./carousel.scss";

export const CarouselBox = ({images, deleteImage}: any) => {
    // @ts-ignore
    return (
        <Carousel>
            {(images && images.length > 0) ? (images?.map( (el: any, i: any) => {
               return  (
                   <CarouselItem>
                    <img id={el.id} className='photo' key={i} loading="lazy" width={400} height={400}
                         src={`${BASE_URL}/user/image/${el.filename}`} alt={el[i]}/>
                    <img onClick={() => deleteImage(i)} className='basket' src={basket}/>
                </CarouselItem>
               )

            })) : null}
        </Carousel>
    )
}
CarouselBox.propTypes = {
    images: PropTypes.array,
    deleteImage: PropTypes.func,
}