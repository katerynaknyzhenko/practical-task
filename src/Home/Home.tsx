import React from "react";
import {CarouselBox} from "./Carousel/CarouselBox";
import {NavLink, useHistory} from "react-router-dom";
import {BaseSyntheticEvent} from 'react';
import {translate} from "../i18n/translate";
import {BASE_URL} from "../constants";
import {Context} from "../Context";
import {ErrorComponent} from "../Error/Error";
import {client, handleErrorsByResponse} from "../api_service/API_client";
import "./home.scss";
import {useSelector} from "react-redux";


// @ts-ignore
export const Home = ({name})  => {
    const [images, setImages] = React.useState<any>([]);
    const [userName, setUserName] = React.useState<string>(name);
    const [userEmail, setUserEmail] = React.useState<string>('');
    const {cookies, setCookie, removeCookie} = React.useContext(Context);
    let history = useHistory();

    const errorFromStore = useSelector((state: any) => state.errors);

    const ifError401 = () => {
        removeCookie('token');
        history.push('/');
    }

    React.useEffect(() => {
        const myData = async() => {
            const headersMe = {'Authorization': cookies?.token && `Bearer ${cookies?.token}`};
            const headersImages = {'Authorization': cookies?.token && `Bearer ${cookies?.token}`,  "type": "formData"};

            const responseUserInfo = await client.get(`${BASE_URL}/user/me`,
                {headers: headersMe, isStoreClean: true}
               )
               const parsedResponse = await responseUserInfo?.json();
               await setUserName(parsedResponse.username);
               setUserEmail(parsedResponse.email)

               const responseImagesInfo = await client.get(`${BASE_URL}/user/image`,
                   {headers: headersImages, isStoreClean: true})

               const ifResponse200_300 = async () => {
                   const parsedResponse = await responseImagesInfo?.json();
                   setImages(parsedResponse);
               }

               await handleErrorsByResponse(responseUserInfo, null, ifError401);
               await handleErrorsByResponse(responseImagesInfo, ifResponse200_300, ifError401);
    }
    myData();
    }, [])


    const carousel = React.useRef<any>();

    const handleImageLoad = async (event: BaseSyntheticEvent) => {
        if (event.target.files && event.target.files[0]) {
            const formData = new FormData();
            for (const key of event.target.files) {
                formData.append('image', key);
            }
            // @ts-ignore
            window.myFormdata = formData;

            const postImgHeaders = {
                'Authorization': cookies?.token && `Bearer ${cookies?.token}`,
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
            }
                const postResponse = await client.post(`${BASE_URL}/user/image`,
                  formData, postImgHeaders
                )
                const ifPostResponseOk = async () => {
                    const parsedPostR = await postResponse?.json();
                    (parsedPostR.length) &&
                    parsedPostR.map((el: string) => setImages([...images, el]))
                }
                await handleErrorsByResponse(
                    postResponse,ifPostResponseOk, ifError401);

                const getHeaders = {'Authorization': cookies?.token && `Bearer ${cookies?.token}`};

                const getResponse = await client.get(`${BASE_URL}/user/image`, {headers: getHeaders,
                    isStoreClean: errorFromStore.length > 0})

                const ifGetResponse200_300 = async () => {
                    const parsedGetR = await getResponse?.json();
                    await setImages(parsedGetR);
                }

                await handleErrorsByResponse(
                    getResponse, ifGetResponse200_300, ifError401);

        }
    }

    const handleImageDelete = (item: number) => {
        const onDelete = async() => {
                const imgToDelete = `${BASE_URL}/user/image/${images[item].filename}`;

                const deleteHeaders = {
                        'Authorization': cookies?.token && `Bearer ${cookies?.token}`,
                        'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                    };

                const deleteResponse = await client.delete(imgToDelete, deleteHeaders)

                await handleErrorsByResponse(
                    deleteResponse,null,ifError401);

                if (imgToDelete === localStorage.getItem('ava')) {
                    localStorage.removeItem('ava');
                }
                const headersGet = {'Authorization': cookies?.token && `Bearer ${cookies?.token}`};

                const getResponse = await client.get(`${BASE_URL}/user/image`, {headers: headersGet, isStoreClean: false})

                const parsedGetResponse = await getResponse?.json();

                const ifGetResponse200_300 = async () => {
                   setImages(parsedGetResponse);

                }
                setImages(parsedGetResponse)
                await handleErrorsByResponse(
                    getResponse, ifGetResponse200_300, ifError401);
        }
        onDelete();

        const last = images[images.length - 1];
        if (images[item] === last) {
            carousel.current.getElementsByClassName('carousel-control-prev')[0].click();
        }
    };


 const handleSelectAvatar = () => {
     const activeItem =
         carousel?.current?.getElementsByClassName('active carousel-item')[0]?.getElementsByClassName('photo')[0].src;
     localStorage.setItem('ava', `${activeItem}`);
 }

    return (
        <div className="main-container">
            <div className="title">{translate('welcome home')}</div>
            <div className="flex-container">
            <div className="flex-child_1">
            <label className="uploading-label">
               <span className="uploading-button">
                  {translate('upload-photo')}
               </span>
            <input className="uploading-input"
                   onChange={handleImageLoad}
                   onClick={(event: BaseSyntheticEvent) => {event.target.value = null}}
                   type="file" name='uploading' title='Upload photo'/>
            </label>
            <div className="carousel-wrapper" ref={carousel}>
            <CarouselBox images={images} deleteImage={handleImageDelete} />
            </div>
                <button onClick={handleSelectAvatar} className='select-button'>
                    {translate('select-avatar')}
                </button>
            </div>
            <div className="flex-child_2">
                <div className="user-info">
                    <ul>
                        <li>{`${userName}`}</li>
                        <li>{`${userEmail}`}</li>
                    </ul>
                    {(errorFromStore.length > 0) && <ErrorComponent errors={errorFromStore}/>}
                </div>
                <div className='link-button'><NavLink to='/chat' style={{textDecoration: 'none'}}>
                    <button className='chat-button'>{translate('ask')}</button>
                </NavLink>
                </div>
            </div>
            </div>
        </div>
    )
}


Home.defaultProps = {
    name: 'user',
}
