import React from 'react';
import {Route} from "react-router-dom";
import {Registration} from "../Registration/Registration";
import {LogIn} from "../Login/LogIn";
import {Home} from "../Home/Home";
import {Chat} from "../Chat/Chat";
import {Provider} from "../i18n/provider";
import {Context} from "../Context";
import {useCookies} from "react-cookie";
import './App.scss';

function App() {
    const [language, setLanguage] = React.useState('en');
    const [cookies, setCookie, removeCookie] = useCookies(['token']);

    return (
        <Context.Provider value={{cookies, setCookie, removeCookie}}>
      <Provider locale={language}>
    <div className="container-parent">
        <Route exact path='/'><LogIn language={language} chooseLanguage={setLanguage}/></Route>
        <Route component={Registration} path='/registration'/>
        <Route component={Home} path='/home'/>
        <Route component={Chat} path='/chat'/>
    </div>
      </Provider>
        </Context.Provider>
  );
}

export default App;
