import '@testing-library/jest-dom';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Enzyme from 'enzyme';
import 'localstorage-polyfill';


Enzyme.configure({ adapter: new Adapter() });

global['localStorage'] = localStorage;

