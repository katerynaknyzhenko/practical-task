import {store} from '../index';

export class APIClient {

    async request(url: string, options: any) {
        try {
            const isRequestFile = JSON.stringify(options.customHeaders).includes
            ('application/json, application/xml, text/plain, text/html, *.*');

            const head: any = isRequestFile ?
                {'Accept': 'application/json, application/xml, text/plain, text/html, *.*', ...options.customHeaders} :
                {'Content-Type': 'application/json;charset=utf-8', ...options.customHeaders}


            const result = await fetch(url, {
                headers: head,
                ...options
            });
            const errorsList: any[] = [];
            switch (true) {
                case (result && (result.status >= 500)): {
                    alert('Unknown error');
                    break;
                }
                case (result && (result?.status >= 400 && result.status < 500)): {
                    const parsedResult = await result?.json();
                    errorsList.push(parsedResult?.message);
                    store.dispatch({type: 'EXACT_ERROR', payload: {errors: errorsList}});
                    break;
                }
                case (result && ((result.status >= 200 && result.status < 300))): {
                    options.isStoreClean && store.dispatch({type: 'DELETE_ERRORS', payload: {errors: errorsList}});
                    break;
                }
            }
            return result;
        } catch (error) {
            console.log('error');
        }
    }


    post(url: string, body: any, options: any) {
        const isRequestFile = JSON.stringify(options).includes
        ('application/json, application/xml, text/plain, text/html, *.*');
        const conditionalBody = isRequestFile ? body : JSON.stringify(body);
        const postOptions = {
            ...options,
            method: 'POST',
            body: conditionalBody,
            customHeaders: options,
        };
        return this.request(url, postOptions);
    }

    get(url: string,  options: any) {
        const getOptions = {
            ...options,
            method: 'GET',
            customHeaders: options.headers,
            isStoreClean: options.isStoreClean,
        }
        const getResponse = this.request(url, getOptions);
        return getResponse;
    }

    delete (url: string, options: any) {
        const deleteOptions = {
            ...options,
            method: 'DELETE',
            customHeaders: options,
        };
        return this.request(url, deleteOptions);
    }

    put(url: string, options: any) {
        const putOptions = {
            ...options,
            method: 'PUT',
            body: JSON.stringify(options)
        };
        return this.request(url, putOptions);

    }

}
export const client = new APIClient();

export const handleErrorsByResponse = (response: Response | undefined, a: any, b: any ) => {
    switch (true) {
        case (response && ((response.status >= 200 && response.status < 300))):
            a && a();
            break;
        case (response && (response.status === 401)):
            b && b();
            break;

    }
}