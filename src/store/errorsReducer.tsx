const ADD_ERRORS = 'ADD_ERRORS';
const DELETE_ERRORS = 'DELETE_ERRORS';
const EXACT_ERROR = 'EXACT_ERROR';

export const errorsReducer = (state = [], action: any) => {
    switch (action.type) {
        case ADD_ERRORS: {
            const item = action.payload.errors;
            return [...state, ...item]
        }
        case DELETE_ERRORS: {
            return []
        }
        case EXACT_ERROR: {
            const item = action.payload.errors;
            return [...item]
        }
        default:
    return state;
    }
}