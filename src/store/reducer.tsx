import {combineReducers} from "redux";
import {errorsReducer} from "./errorsReducer";

export const rootReducer = combineReducers({errors: errorsReducer});