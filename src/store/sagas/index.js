import {takeEvery, call, all} from 'redux-saga/effects';
import {spawn} from "child_process";

export function* workerSaga () {
    console.log('click from wqtcher - some api calls');
    console.log('Youtube channel wise.js - Redux-Saga React полный курс');
}

export function* watcherSaga () {
    yield takeEvery('EXACT_ERROR', workerSaga);
}

export default function* rootSaga () {
    const sagas = [
        watcherSaga
    ]
    const retrySagas = yield sagas.map(saga => {
        return spawn(function* () {
            while (true) {
                try {
                    yield call(saga)
                } catch (e) {
                    console.log(e);
                }

            }
    })
})
yield all(retrySagas)}
