import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App/App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Provider} from "react-redux";
import {createStore} from "redux";
import {rootReducer} from './store/reducer';
import {applyMiddleware, compose} from "redux";
import createSagaMiddleware from 'redux-saga';
import rootSaga from "./store/sagas";

const sagaMiddleware = createSagaMiddleware();
declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer,
    composeEnhancers (
    applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <BrowserRouter>
      <Route path='/'>
          <Provider store={store}>
          <App/>
          </Provider>
      </Route>
  </BrowserRouter>,
  document.getElementById('root')
);
reportWebVitals();
